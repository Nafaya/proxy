var _ = require('underscore') ;
const config = _.defaults(require('./config')||{},{
	basic:{
		realm:"Nafaya Area.",
		login:"",
		password:""
	},
	port:9091
}) ;
var http = require('http') ;
var net = require('net') ;
var url = require('url') ;
var auth = require('http-auth');
var basic = auth.basic({
        realm: config.realm
    }, function (username, password, callback) { // Custom authentication method.
        callback(!config.basic || (username === config.basic.login && password === config.basic.password));
    }
);
var server = http.createServer(basic,function(request, response) {
	var ph = url.parse(request.url) ;
	var options = {
		port: ph.port,
		hostname: ph.hostname,
		method: request.method,
		path: ph.path,
		headers: request.headers
	} ;
	var proxyRequest = http.request(options) ;
	proxyRequest.on('response', function(proxyResponse) {
		proxyResponse.on('data', function(chunk) {
			response.write(chunk, 'binary')
		}) ;
		proxyResponse.on('end', function() { response.end() }) ;
		response.writeHead(proxyResponse.statusCode, proxyResponse.headers)
	}) ;
	proxyRequest.on('error',function(e){
		request.destroy() ;
	}) ;
	request.on('data', function(chunk) {
		proxyRequest.write(chunk, 'binary')
	}) ;
	request.on('end', function() { proxyRequest.end();}) ;
	request.on('error', function(e) {
		proxyRequest.end() ;
	})
}).on('connect', function(request, socketRequest, head) {
    var ph = url.parse('http://' + request.url) ;
    var socket = net.connect(ph.port, ph.hostname, function() {
        socket.write(head)
        // Сказать клиенту, что соединение установлено
        socketRequest.write("HTTP/" + request.httpVersion + " 200 Connection established\r\n\r\n")
    }) ;
    // Туннелирование к хосту
    socket.on('data', function(chunk) { socketRequest.write(chunk); }) ;
    socket.on('end', function() { socketRequest.end() ;console.log('G2');}) ;
    socket.on('error', function(e) {
        // Сказать клиенту, что произошла ошибка
	//console.log('[57]');
	//console.log(e) ;
        socketRequest.write("HTTP/" + request.httpVersion + " 500 Connection error\r\n\r\n") ;
        socketRequest.end()
    }) ;
    // Туннелирование к клиенту
    socketRequest.on('data', function(chunk) { socket.write(chunk); }) ;
    socketRequest.on('end', function() { socket.end() ;}) ;
    socketRequest.on('error', function() { socket.end() ;})
    //}catch(e){}
}).listen(config.port) ;
server.on('error',function(e){
    console.log(e);
}) ;
console.log('good') ;